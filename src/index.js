const express = require('express');
const bodyParser = require('body-parser');
const middleware_cors = require('./middlewares/cors');
const port = 2000;

// Express application
const app = express();

// Body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Middleware allowing all for testing purposes (methods, cross-origin, ..)
app.use(middleware_cors.allowAll);

// Requiring express.Router() modules to assign in routes
const api_samples = require('./routes/samples');
const api_swagger = require('./routes/swagger');

// Assigning routes to imported routers
app.use('/', api_swagger);
app.use('/samples/', api_samples);

// Default error handler
app.use((err, req, res, next) => res.status(500).send('Something went wrong!'));

// Deny access for any route not mapped
app.use((req, res) => res.status(403).json({ error: 'Forbidden access! URL not allowed.' }));

// Starting application server listening to it's port
app.listen(port, () => console.log(`Server listening to ${port}`));
