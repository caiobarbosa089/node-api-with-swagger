const router = require('express').Router();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../config/swagger.json');

router.use('/', swaggerUi.serve); // Swagger middleware
router.get('/', swaggerUi.setup(swaggerDocument)); // Swagger output

module.exports = router;
