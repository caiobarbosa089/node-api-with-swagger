const express = require('express');

// Initiating router
const router = express.Router(); // Used to assign into a Node server route

/** Sample for get */
router.get('/:a/:b', (req, res) => {
	let a = req.params.a || null;
  let b = req.params.b || null;
  res.json({ success: true, data: { a, b } });
})

/** Sample for post */
router.post('/', (req, res) => {
	let { a, b } = req.body;
  res.json({ success: true, data: { a, b } });
});

/** Sample for put */
router.put('/', (req, res) => {
	let { a, b } = req.body;
	  res.json({ success: true, data: { a, b } });
});

/** Sample for delete */
router.delete('/:a/:b', (req, res) => {
	  res.json({ success: true });
});

module.exports = router; // Exporting as a default router
