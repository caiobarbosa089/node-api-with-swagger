# node-api-with-swagger
## About
RESTful API built using Node + Express technologies.
Working with Swagger for manipulation of API.

[Edit on Repl.it ⚡️](https://repl.it/@caio002barbosa/node-api-with-swagger)

## How to use
To run the application you must have installed NodeJS and npm, click [here](https://nodejs.org/) to go to the offical website and download.

With everything configured, you must open your personal terminal and go to the `src` folder.

Run `npm i` to intall all the dependecies. This process may take a while.

Run `npm start` to start the application and that's all.

To stop the running application press `ctrl + c` and `S` after.
